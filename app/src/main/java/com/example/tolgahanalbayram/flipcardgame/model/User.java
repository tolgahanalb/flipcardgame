package com.example.tolgahanalbayram.flipcardgame.model;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

public class User {
    private String userName;
    private long highScore;
    private long totalScore;

    public User() {
    }

    public User(String userName, long highScore, long totalScore) {
        this.userName = userName;
        this.highScore = highScore;
        this.totalScore = totalScore;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public long getHighScore() {
        return highScore;
    }

    public void setHighScore(long highScore) {
        this.highScore = highScore;
    }

    public long getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(long totalScore) {
        this.totalScore = totalScore;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("userName", userName);
        result.put("highScore", highScore);
        result.put("totalScore", totalScore);

        return result;
    }
}
