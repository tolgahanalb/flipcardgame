package com.example.tolgahanalbayram.flipcardgame.widget;

import android.app.Dialog;
import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.tolgahanalbayram.flipcardgame.R;

import org.apache.commons.lang3.StringUtils;

public class CustomInfoDialog {
    private Dialog mDialog;
    private Context mContext;
    private InfoDialogButtonListener buttonListener;

    public CustomInfoDialog(Context mContext, InfoDialogButtonListener buttonListener) {
        this.mContext = mContext;
        this.buttonListener = buttonListener;
    }

    public void show() {
        mDialog = new Dialog(mContext);

        mDialog.setContentView(R.layout.login_dialog);
        mDialog.findViewById(R.id.login_dialog_mainLL).setLayoutParams(new FrameLayout.LayoutParams(900, ViewGroup.LayoutParams.WRAP_CONTENT));

        TextInputEditText nickNameEditText = mDialog.findViewById(R.id.et_login_dialog);

        Button loginButton = mDialog.findViewById(R.id.button_login_dialog);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = nickNameEditText.getText().toString();
                userName = userName.replaceAll("\\s+", "");
                if (userName.equals(StringUtils.EMPTY)) {
                    Toast.makeText(mContext, R.string.wrong_username, Toast.LENGTH_LONG).show();
                } else {
                    buttonListener.onButtonClicked(userName);
                    mDialog.dismiss();
                }

            }
        });
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.show();
    }


    public interface InfoDialogButtonListener {
        void onButtonClicked(String userName);
    }
}