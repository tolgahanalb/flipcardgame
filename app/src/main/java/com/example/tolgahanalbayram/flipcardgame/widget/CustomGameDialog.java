package com.example.tolgahanalbayram.flipcardgame.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tolgahanalbayram.flipcardgame.R;

public class CustomGameDialog {
    private Dialog mDialog;
    private Context mContext;
    private TextView tvRating;
    private TextView tvScore;
    private Button okButton;
    private ImageView ivGameDialog;
    private GameDialogButtonListener gameDialogButtonListener;


    public CustomGameDialog(Context mContext, GameDialogButtonListener gameDialogButtonListener) {
        this.mContext = mContext;
        this.gameDialogButtonListener = gameDialogButtonListener;
    }

    public void show(int score, String rating,int imageResource) {

        mDialog = new Dialog(mContext);
        mDialog.setContentView(R.layout.game_dialog);
        mDialog.findViewById(R.id.game_dialog_mainLL).setLayoutParams(new FrameLayout.LayoutParams(900, ViewGroup.LayoutParams.WRAP_CONTENT));
        ivGameDialog= mDialog.findViewById(R.id.iv_game_info_dialog);
        tvRating = mDialog.findViewById(R.id.tv_rating_game_dialog);
        tvScore = mDialog.findViewById(R.id.tv_score_game_dialog);
        okButton = mDialog.findViewById(R.id.button_game_dialog);
        ivGameDialog.setBackgroundResource(imageResource);
        tvRating.setText(rating);
        tvScore.setText(String.valueOf(score));
    okButton.setText("Ana Sayfaya Dön");
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gameDialogButtonListener.onClickOkayButton(okButton,tvScore);
                mDialog.cancel();

            }
        });
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.show();
    }

    public void setHighScoreText(String text){}
    public interface GameDialogButtonListener {
        void onClickOkayButton(Button button, TextView textView);
    }
}
