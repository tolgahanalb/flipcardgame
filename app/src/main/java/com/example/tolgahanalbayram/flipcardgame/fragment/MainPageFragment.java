package com.example.tolgahanalbayram.flipcardgame.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.tolgahanalbayram.flipcardgame.MainActivity;
import com.example.tolgahanalbayram.flipcardgame.R;
import com.example.tolgahanalbayram.flipcardgame.helper.SharedPrefHelper;
import com.example.tolgahanalbayram.flipcardgame.model.User;
import com.example.tolgahanalbayram.flipcardgame.widget.CustomInfoDialog;
import com.example.tolgahanalbayram.flipcardgame.widget.CustomInfoDialog.InfoDialogButtonListener;

import org.apache.commons.lang3.StringUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainPageFragment extends Fragment implements InfoDialogButtonListener {
    @BindView(R.id.button_login_fragment_main_page)
    Button loginButton;
    @BindView(R.id.tv_nickname_fragment_main_page)
    TextView nickNameTv;
    @BindView(R.id.tv_highscore_fragment_main_page)
    TextView highScoreTv;
    @BindView(R.id.tv_totalscore_fragment_main_page)
    TextView totalScoreTv;
    @BindView(R.id.mainpage_values_ll)
    LinearLayout userValuesLl;
    @BindView(R.id.tv_welcome_fragment_main_page)
    TextView welcomeMessageTv;
    @BindView(R.id.iv_play_fragment_main_page)
    ImageView playIv;

    public MainPageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MainActivity) getActivity()).getTotalScore();
        ((MainActivity) getActivity()).getTotalScore();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_page, container, false);
        ButterKnife.bind(this, view);
        if (SharedPrefHelper.getStringFromShared(getContext(), "UserId") == null) {
            userValuesLl.setVisibility(View.INVISIBLE);
        } else {
            userValuesLl.setVisibility(View.VISIBLE);
            loginButton.setVisibility(View.GONE);
            welcomeMessageTv.setVisibility(View.VISIBLE);
            ((MainActivity) getActivity()).getUserValuesFromFirebase(nickNameTv, highScoreTv, totalScoreTv);
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @OnClick(R.id.button_login_fragment_main_page)
    void loginButtonClicked() {
        new CustomInfoDialog(getContext(), this).show();
    }

    public void setUserValuesToViews(String userName, long highScore, long totalScore) {
        nickNameTv.setText(getResources().getString(R.string.main_page_nickname, userName));
        highScoreTv.setText(getResources().getString(R.string.main_page_highscore, highScore));
        totalScoreTv.setText(getResources().getString(R.string.main_page_totalscore, totalScore));
    }

    @Override
    public void onButtonClicked(String userName) {
        if(!userName.equals(StringUtils.EMPTY)){
        User user = new User(userName, 0, 0);
        ((MainActivity) getActivity()).writeNewUsertoDb(user);
        setUserValuesToViews(userName, 0, 0);
        loginButton.setVisibility(View.GONE);
        userValuesLl.setVisibility(View.VISIBLE);
        welcomeMessageTv.setVisibility(View.VISIBLE);}

    }

    @OnClick(R.id.iv_play_fragment_main_page)
    void playButtonClicked() {
        if (SharedPrefHelper.getStringFromShared(getContext(), "UserId") == null) {
            new CustomInfoDialog(getContext(), this).show();

        } else {
            ((MainActivity) getActivity()).replace(new PlayFragment());

        }
    }



}
