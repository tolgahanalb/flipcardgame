package com.example.tolgahanalbayram.flipcardgame.model;

import java.util.ArrayList;

public class GameObject {

    private String id;
    private int resourceId;


    public GameObject(String id, int resourceId) {
        this.id = id;
        this.resourceId = resourceId;
    }

    public ArrayList<GameObject> GameObject(GameObject gameObject) {
        ArrayList<GameObject> cloneGameObject = new ArrayList<>() ;
        cloneGameObject.add(gameObject);
        cloneGameObject.clone();
        return cloneGameObject;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }
}
