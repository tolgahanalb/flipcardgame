package com.example.tolgahanalbayram.flipcardgame.fragment;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.tolgahanalbayram.flipcardgame.MainActivity;
import com.example.tolgahanalbayram.flipcardgame.R;
import com.example.tolgahanalbayram.flipcardgame.adapter.GameItemAdapter;
import com.example.tolgahanalbayram.flipcardgame.model.GameObject;
import com.example.tolgahanalbayram.flipcardgame.widget.CustomGameDialog;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlayFragment extends Fragment implements GameItemAdapter.GameItemAdapterListener, CustomGameDialog.GameDialogButtonListener {

    @BindView(R.id.rv_fragment_game)
    RecyclerView rvCardList;
    @BindView(R.id.tv_score_game)
    TextView tvScore;
    @BindView(R.id.tv_timer_game)
    TextView tvTimer;
    @BindView(R.id.tv_move_game)
    TextView tvMove;


    public ArrayList<GameObject> itemList = new ArrayList<>();
    protected View fragmentContent;
    private GameItemAdapter adapter;
    private int level = 1;
    private Stack<String> openCardStack = new Stack<>();
    private String secondCardId = StringUtils.EMPTY;
    private String firstCardId = StringUtils.EMPTY;
    private HashMap<View, View> openedItems = new HashMap<>();
    private boolean isClickable = Boolean.TRUE;
    private ArrayList<String> matchedCards = new ArrayList<>();
    private int matchedCardCount;
    private int score;
    private long timeBonus;
    private int hamle;
    private CountDownTimer countDownTimer;
    private Boolean isGameOver = Boolean.FALSE;


    public PlayFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentContent = inflater.inflate(R.layout.fragment_game, container, false);

        return fragmentContent;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, fragmentContent);
        setRecyclerView();

    }

    public void setMove() {
        tvMove.setText(String.valueOf(hamle));
    }

    public void setScore() {
        switch (level) {
            case 1:
                score += 5;
                break;
            case 2:
                score += 6;
                break;
            case 3:
                score += 7;
                break;
            case 4:
                score += 8;
                break;

        }
        tvScore.setText(String.valueOf(score));

    }

    public int endGameScoreCalculation() {
        int endGameScore = (int) (score + (hamle * timeBonus));
        return endGameScore;
    }

    public void gameOver() {
        onStop();
        onDestroy();
        countDownTimer.cancel();

        new CustomGameDialog(getContext(), this).show(0, "Game Over", R.drawable.gameoverimage);

    }

    private void finishWithSuccess() {
        final long firstScore = ((MainActivity) getActivity()).getTotalScore();
         int a = endGameScoreCalculation();
        long totalscore = firstScore + a;
        ((MainActivity) getActivity()).setTotalScore("totalScore", totalscore);
        long highScore = ((MainActivity) getActivity()).getHighScore();
        if (a > highScore) {
            new CustomGameDialog(getContext(), this).show(a, "High Score", R.drawable.highscoreimage);
            ((MainActivity) getActivity()).setTotalScore("highScore", a);
        } else {
            new CustomGameDialog(getContext(), this).show(a, "Tebrikler", R.drawable.happyimage);
        }

        countDownTimer.cancel();
    }

    public void setTimer() {
        countDownTimer = new CountDownTimer(180000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                int minutes = (int) TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
                int seconds = (int) (TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
                tvTimer.setText(String.format("%02d:%02d", minutes, seconds));
                timeBonus = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) / 10;
            }

            @Override
            public void onFinish() {
                gameOver();
            }
        }.start();
    }


    @SuppressLint("ClickableViewAccessibility")
    public void setRecyclerView() {
        createGameObject();
        rvCardList.setLayoutManager(new GridLayoutManager(getContext(), getSpanCount()));

        adapter = new GameItemAdapter(itemList, this);
        rvCardList.setAdapter(adapter);


    }

    public int getSpanCount() {
        switch (level) {
            case 1:
                return 3;
            case 2:
                return 4;
            case 3:
                return 3;

            case 4:
                return 4;

            default:
                return 3;
        }
    }

    public void createGameObject() {

        int ba;
        switch (level) {
            case 1:
                ba = 3;
                matchedCardCount = 6;
                hamle = 6;

                break;
            case 2:
                ba = 4;
                matchedCardCount = 8;
                hamle = 10;
                break;
            case 3:
                ba = 6;
                matchedCardCount = 12;
                hamle = 14;
                break;
            case 4:
                ba = 8;
                matchedCardCount = 16;
                hamle = 20;
                break;
            default:
                ba = 3;
                break;

        }
        itemList.clear();
        matchedCards.clear();
        tvMove.setText(String.valueOf(hamle));

        for (int i = 0; i < ba; i++) {
            int image = getContext().getResources().getIdentifier("animal_" + i, "drawable", getContext().getPackageName());
            GameObject gameObject = new GameObject(String.valueOf(i), image);
            fill(gameObject);
        }


    }

    public ArrayList<GameObject> fill(GameObject gameObject) {
        itemList.add(gameObject);
        ArrayList<GameObject> cloneArray = gameObject.GameObject(gameObject);
        itemList.addAll(cloneArray);
        Collections.shuffle(itemList);
        return itemList;
    }

    private void closeOpenedCards() {

        for (Map.Entry<View, View> card : openedItems.entrySet()) {

            resetCards(card.getKey(), card.getValue());
            card.getKey().setTag(true);
        }
        openedItems.clear();
    }

    private void resetCards(View closedCardSide, View openedCardSide) {

        closedCardSide.setTag(false);   //kart i kapali olarak isaretle

        AnimatorSet mRightOut;
        AnimatorSet mLeftIn;

        mRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.out_animation);
        mLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.in_animation);
//        mLeftIn.setStartDelay(400);
//        mRightOut.setStartDelay(400);
        mRightOut.setTarget(openedCardSide);
        mLeftIn.setTarget(closedCardSide);
        mRightOut.start();
        mLeftIn.start();

    }

    public void openCard(View frontView, View backView) {
        backView.setTag(false);    //kart i acildi olarak isaretle
        frontView.setVisibility(View.VISIBLE);
        AnimatorSet mRightOut;
        AnimatorSet mLeftIn;
        mLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.in_animation);
        mRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.out_animation);

        mLeftIn.setTarget(frontView);
        mLeftIn.start();
        mRightOut.setTarget(backView);
        mRightOut.start();

    }

    public boolean isCardsMatched() {

        secondCardId = openCardStack.get(1);
        firstCardId = openCardStack.get(0);
        return StringUtils.equals(firstCardId, secondCardId);
    }

    public void removeCards() {
        isClickable = Boolean.FALSE;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                for (Map.Entry<View, View> card : openedItems.entrySet()) {
                    card.getKey().setVisibility(View.GONE);
                    card.getValue().setVisibility(View.GONE);

                    isClickable = Boolean.TRUE;
                }
                openedItems.clear();
            }
        }, 450);


    }

    @Override
    public void onGameItemClicked(View backView, View imageFrontView, String cardId) {


        if ((backView.getTag() == Boolean.TRUE || backView.getTag() == null) && (isClickable)) {

            openCard(imageFrontView, backView);
            openCardStack.push(cardId);
            if (openCardStack.size() == 2) {
                hamle -= 1;
                setMove();
                if (isCardsMatched()) {
                    matchedCards.addAll(openCardStack);
                    removeCards();
                    openCardStack.clear();
                    setScore();
                }
                if (hamle == 0) {
                    isGameOver = Boolean.TRUE;
                    gameOver();
                }

            }

            if (openCardStack.size() == 3) {
                closeOpenedCards();
                openCardStack.clear();
                openCardStack.push(cardId);
            }
            openedItems.put(backView, imageFrontView);
        }
        if (matchedCardCount == matchedCards.size()) {
            if (level < 4 && !isGameOver) {
                nextLevel();
            } else if (!isGameOver){
                finishWithSuccess();


            }
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d("PlayFragment", "onResume()");

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("PlayFragment", "onDestroy()");
        countDownTimer.cancel();


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        setTimer();
    }

    public void nextLevel() {
        level += 1;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setRecyclerView();
            }
        }, 650);


    }

    @Override
    public void onClickOkayButton(Button buton, TextView tv) {
        ((MainActivity) getActivity()).replace(new MainPageFragment());


    }


}