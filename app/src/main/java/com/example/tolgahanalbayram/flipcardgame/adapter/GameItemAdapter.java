package com.example.tolgahanalbayram.flipcardgame.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.example.tolgahanalbayram.flipcardgame.R;
import com.example.tolgahanalbayram.flipcardgame.model.GameObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GameItemAdapter extends RecyclerView.Adapter<GameItemAdapter.ItemViewHolder> {
    public ArrayList<GameObject> gameObjects = new ArrayList<>();
    public GameItemAdapterListener gameItemAdapterListener;

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if ( gameItemAdapterListener != null) {
                GameItemTag tag = (GameItemTag) view.getTag();
                performClick(tag.itemViewHolder, tag.itemId);
            }
        }
    };
    public GameItemAdapter(ArrayList<GameObject> gameObjects, GameItemAdapterListener gameItemAdapterListener) {
        this.gameObjects = gameObjects;
        this.gameItemAdapterListener = gameItemAdapterListener;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_game_item_container, viewGroup, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder itemViewHolder, int position) {
        final ItemViewHolder holder = (ItemViewHolder) itemViewHolder;
        final GameObject item = getItem(position);
        holder.gameItemFrontIv.setImageResource(gameObjects.get(position).getResourceId());
        holder.gameItemFrontIv.setVisibility(View.INVISIBLE);
        holder.gameItemContainerFl.setTag(new GameItemTag(itemViewHolder,item.getId()));
        holder.gameItemContainerFl.setOnClickListener(clickListener);


    }

    private GameObject getItem(int position) {
        return gameObjects.get(position);
    }


    private class GameItemTag {
        ItemViewHolder itemViewHolder;
        String itemId;

        GameItemTag(ItemViewHolder itemViewHolder, String itemId) {
            this.itemViewHolder = itemViewHolder;
            this.itemId = itemId;
        }
    }

    @Override
    public int getItemCount() {
        return gameObjects.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.fl_game_item_container)
        FrameLayout gameItemContainerFl;
        @BindView(R.id.iv_front_game_item_container)
        ImageView gameItemFrontIv;
        @BindView(R.id.iv_back_game_item_container)
        ImageView gameItemBackIv;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void performClick(ItemViewHolder itemViewHolder, String cardId) {
        gameItemAdapterListener.onGameItemClicked(itemViewHolder.gameItemBackIv, itemViewHolder.gameItemFrontIv, cardId);
    }

    public interface GameItemAdapterListener {
        void onGameItemClicked(View backView, View imageFrontView, String cardId);
    }
}
