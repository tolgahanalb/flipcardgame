package com.example.tolgahanalbayram.flipcardgame;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.example.tolgahanalbayram.flipcardgame.fragment.MainPageFragment;
import com.example.tolgahanalbayram.flipcardgame.helper.SharedPrefHelper;
import com.example.tolgahanalbayram.flipcardgame.model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private DatabaseReference mDatabase;
    private FragmentManager mFragmentManager = getSupportFragmentManager();
    private MainPageFragment mainPageFragment = new MainPageFragment();
    private long totalScore;
    private long highScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        getHighScore();
        replace(mainPageFragment);
    }

    public void writeNewUsertoDb(User user) {

        final String key = mDatabase.child("users/").push().getKey();
        Map<String, Object> userValues = user.toMap();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("users/user" + key, userValues);
        mDatabase.updateChildren(childUpdates);
        SharedPrefHelper.putStringToShared(getApplicationContext(), "UserId", "user" + key);

    }


    public void replace(Fragment newFragment) {
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.replace(R.id.main_frame, newFragment, newFragment.getClass().getName());
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public long getHighScore() {
        String key = SharedPrefHelper.getStringFromShared(getApplicationContext(), "UserId");

        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Iterable<DataSnapshot> asdas = snapshot.getChildren();
                    for (DataSnapshot dataSnapshot1 : asdas) {
                        User c = dataSnapshot1.getValue(User.class);
                        String a = dataSnapshot1.getKey();
                        if (a.equals(key)) {
                            highScore = c.getHighScore();

                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("bağlanamadı", "firebase");
            }

        };
        mDatabase.addListenerForSingleValueEvent(eventListener);

        return highScore;
    }

    public void getUserValuesFromFirebase(TextView nnTV, TextView hsTV, TextView tsTV) {
        final String pKey = SharedPrefHelper.getStringFromShared(getApplicationContext(), "UserId");

        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Iterable<DataSnapshot> asdas = snapshot.getChildren();
                    for (DataSnapshot dataSnapshot1 : asdas) {
                        User c = dataSnapshot1.getValue(User.class);
                        String a = dataSnapshot1.getKey();
                        if (a.equals(pKey)) {
                            nnTV.setText(getResources().getString(R.string.main_page_nickname, c.getUserName()));
                            hsTV.setText(getResources().getString(R.string.main_page_highscore, c.getHighScore()));
                            tsTV.setText(getResources().getString(R.string.main_page_totalscore, c.getTotalScore()));
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("bağlanamadı", "firebase");
            }

        };
        mDatabase.addListenerForSingleValueEvent(eventListener);

//        mDatabase.addChildEventListener(new ChildEventListener() {
//
//            @Override
//            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//                user = dataSnapshot.getValue(User.class);
//
//                nnTV.setText(getResources().getString(R.string.main_page_nickname, user.getUserName()));
//                hsTV.setText(getResources().getString(R.string.main_page_highscore, user.getHighScore()));
//                tsTV.setText(getResources().getString(R.string.main_page_totalscore, user.getTotalScore()));
//            }
//
//            @Override
//            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
//            }
//
//            @Override
//            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//
//        });

    }

    public void setTotalScore(String Score, long scoreValue) {
        String pKey = SharedPrefHelper.getStringFromShared(getApplicationContext(), "UserId");
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                mDatabase.child("users").child(pKey).child(Score).setValue(scoreValue);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });
    }

    public long getTotalScore() {
        String key = SharedPrefHelper.getStringFromShared(getApplicationContext(), "UserId");

        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Iterable<DataSnapshot> asdas = snapshot.getChildren();
                    for (DataSnapshot dataSnapshot1 : asdas) {
                        User c = dataSnapshot1.getValue(User.class);
                        String a = dataSnapshot1.getKey();
                        if (a.equals(key)) {
                            totalScore = c.getTotalScore();

                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("bağlanamadı", "firebase");
            }

        };
        mDatabase.addListenerForSingleValueEvent(eventListener);

        return totalScore;
    }

}
